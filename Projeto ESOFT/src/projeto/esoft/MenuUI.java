package projeto.esoft;

import java.io.IOException;
import utils.*;

/**
 *
 * @author Nuno Silva
 */
public class MenuUI {

    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa) {
        m_empresa = empresa;
    }

    public void run() throws IOException {
        do {
            //opcao = "1";
            System.out.println("MENU:\n");
            System.out.println("1. Registar utilizador");
            System.out.println("2. Criar evento cientifico");

            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            switch (opcao) {
                case "1":
                    RegistarUtilizadorUI uiRU = new RegistarUtilizadorUI(m_empresa);
                    uiRU.run();
                    break;
                    
                case "2":
                    CriarEventoUI criaEvento = new CriarEventoUI (m_empresa);
            }

        } while (!opcao.equals("0"));
    }
}
