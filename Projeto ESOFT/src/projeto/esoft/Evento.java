
package projeto.esoft;

/**
 *
 * @author Emanuel Marques
 */
public class Evento {

    private String titulo;
    private String dataInicio;
    private String dataFim;

    public Evento(String titulo, String dataInicio, String dataFim) {
        this.titulo = titulo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;

    }

    public String getTitulo() {
        return titulo;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

}
