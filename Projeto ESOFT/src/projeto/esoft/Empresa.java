package projeto.esoft;


import java.util.*;

/**
 *
 * @author Nuno Silva
 */
public class Empresa
{
    private final List<Utilizador> m_listaUtilizadores;

    public Empresa()
    {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        
    }

    public Utilizador novoUtilizador()
    {
        return new Utilizador();
    }
    
    public boolean registaUtilizador(Utilizador u)
    {
        if( u.valida() && validaUtilizador(u) )
            return addUtilizador(u);
        else
            return false;
    }
    
    private boolean validaUtilizador(Utilizador u)
    {
        System.out.println("Empresa: validaUtilizador: " + u.toString());
        return true;
    }
    
    private boolean addUtilizador(Utilizador u)
    {
        return m_listaUtilizadores.add(u);
    }

}